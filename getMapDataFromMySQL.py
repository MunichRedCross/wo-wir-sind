#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb
import re

TAG_RE = re.compile(r'<[^>]+>')

def replace_linebreaks_with_br(str):
	str = '<br>'.join(str.splitlines())
	str = TAG_RE.sub('', str)
	
	return str

db = MySQLdb.connect(host="localhost",  # your host, usually localhost
                     user="",         	# your username
                     passwd="",         # your password
                     db="")             # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cur = db.cursor()

# Use all the SQL you like
cur.execute("SELECT * FROM tx_brkgooglemaps_units")

print '['

# print all the first cell of all the rows
for row in cur.fetchall():
	name = row[9]
	contact = row[18]
	mail = row[17]
	raw_link = row[13]
	link =  raw_link if raw_link.startswith('http://') or raw_link.startswith('https://') else 'http://' + raw_link
	image = row[15]
	description = replace_linebreaks_with_br(row[12])
	raw_address = row[11]
	address = ', '.join(raw_address.splitlines())
	coords = row[10]
	print '{"level":{"society":"Deutsches Rotes Kreuz","state":"Bayerisches Rotes Kreuz","district":"Oberbayern","county":"München","local":"' + name + '","community":"Bereitschaften"},"information":{"contact":"' + contact + '","contactUrl":"https://www.bereitschaften.brk-muenchen.de/mach-mit/kontaktaufnahme/kontaktformular/?tx_powermail_pi1%5Buid125%5D=Karte%20' + name + '&tx_powermail_pi1%5Buid126%5D=' + mail +'","website":"' + link + '","image":"' + image + '","description":"' + description + '"},"address":{"houseName":"' + name + '","street":"' + address + '","houseNumber":"","postcode":"","city":"","coordinates":[' + coords + ']}}, '

print ']'

db.close()
