# Proof of Concept: Wo Wir Sind 7.0

Displays volunteer units of the Munich Red Cross on a map and provides an easy way to contact them.

## Requirements

MySQL table `tx_brkgooglemaps_units` of Typo3 Extension `brk_googlemaps`.

## Usage

1. open `getMapDataFromMySQL.py`
1. add credentials of the database.
1. execute `getMapDataFromMySQL.py` 
1. copy output (JSON-Array) to `index.html` (see below)
1. save and open index.html

**Example:**

~~~
    <script type="text/javascript">
        const mdata = [
			{"level":{"society":"Deutsches Rotes Kreuz","state":"Bayerisches Rotes Kreuz","district":"Oberbayern","county":"München","local":"Bereitschaft Solln","community":"Bereitschaften"},"information":{"contact":"Gertraud Peplow","contactUrl":"https://www.bereitschaften.brk-muenchen.de/mach-mit/kontaktaufnahme/kontaktformular/?tx_powermail_pi1%5Buid125%5D=Karte%20Bereitschaft Solln&tx_powermail_pi1%5Buid126%5D=solln","website":"http://kleiderkammer.bereitschaften.brk-muenchen.de","image":"Bereitschaftsraum.jpg","description":""},"address":{"houseName":"Bereitschaft Solln","street":"Drygalksi Allee 118, 81477 München","houseNumber":"","postcode":"","city":"","coordinates":[48.08258,11.504817]}}, 
		];
		
		...
~~~

## Credits

1. https://leafletjs.com/
1. [Map data © OpenStreetMap contributors, CC-BY-SA](https://www.openstreetmap.org/)

